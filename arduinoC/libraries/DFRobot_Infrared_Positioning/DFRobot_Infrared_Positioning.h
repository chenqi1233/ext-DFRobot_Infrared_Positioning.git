#ifndef __DFRobot_INFRARED_POSITIONING_H__
#define __DFRobot_INFRARED_POSITIONING_H__

#define           ADDRESS                0x58     		// I2C slave Address
#define           IR_DATA_REGISTER       0x36           // Infrared data register

#define           OCOUNT                 16             // Infrared Count Value
class DFRobot_Infrared_Positioning{  
public:
	
	DFRobot_Infrared_Positioning();
	~DFRobot_Infrared_Positioning();
	 
	void     initialize();
	int    ReadInfraredData(uint8_t DataNum);
private:	
	void     i2cWrite(uint8_t Reg , uint8_t pdata);
	int    InfraredData[OCOUNT] ;
	 

};
#endif