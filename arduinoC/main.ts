//% color="#4169E1" iconWidth=50 iconHeight=40
namespace DFRobot_Infrared_Positioning{

    //% block="Initializing the infrared positioning probe" blockType="command" 
 
    export function DFRobot_Infrared_PositioningInit(parameter: any, block: any) {
         
     

        Generator.addInclude("DFRobot_Infrared_Positioning", "#include <DFRobot_Infrared_Positioning.h>");
        Generator.addInclude("DFRobot_Infrared_Positioning1", "#include <Wire.h>");
        Generator.addObject("DFRobot_Infrared_Positioning","DFRobot_Infrared_Positioning ",`IR;`);
  
        Generator.addSetup(`DFRobot_Infrared_Positioningnit`, `IR.initialize();`);
        Generator.addSetup(`DDFRobot_Infrared_PositioningInit1`, `Wire.begin();`);
        
        
    }
    //% block="Get the infrared beacon position [NUM]" blockType="reporter" 
    //% NUM.shadow="dropdown" NUM.options="NUM"
    export function DFRobot_Infrared_PositioningReady(parameter: any, block: any) { 
        let num=parameter.NUM.code;

        Generator.addCode( [`IR.ReadInfraredData(${num})`,Generator.ORDER_UNARY_POSTFIX]);
   
   }
    
}