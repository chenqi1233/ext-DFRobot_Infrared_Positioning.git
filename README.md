# 红外定位探头 

红外定位摄像头可追踪4个移动的红外点，分辨率是1024 X 768

![](./arduinoC/_images/featured.png)

# 积木

![](./arduinoC/_images/blocks.png)

# 程序实例

![](./arduinoC/_images/example.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|-----|-----|
|uno||√|||
|micro:bit||√|||
|mpython||√|||
|arduinonano||√|||
|leonardo||√|||
|mega2560||√|||
|firebeetleesp32||√|||
|telloesp32||√||| 
 

# 更新日志

V0.0.1 基础功能完成

